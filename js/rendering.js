rendering = function(canvas, sp) {
    var ctx = canvas.getContext('2d');

    var padZoneLineWidth = 5;

    var draw = function() {
        drawTable();
        drawPuck(sp.puck);
        drawPlayer(sp.padPlayerLeft);
        drawPlayer(sp.padPlayerRight);
    };

    var drawTable = function() {
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, sp.table.w, sp.table.h);

        ctx.fillStyle = 'green';
        ctx.fillRect(sp.table.padZoneH, 0, padZoneLineWidth, sp.table.h);
        ctx.fillRect(sp.table.w - sp.table.padZoneH - padZoneLineWidth, 0, padZoneLineWidth, sp.table.h);
    };

    var drawPuck = function(puck) {
        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc(puck.x, puck.y, puck.getR(), 0, Math.PI * 2, false);
        ctx.fill();
    };

    var drawPlayer = function(p) {
        ctx.fillStyle = 'blue';
        ctx.fillRect(p.x, p.y, p.getW(), p.getH());
    };

    return {
        draw: draw
    };

};
