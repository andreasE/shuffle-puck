handleEvents = function(canvas, sp) {
    
    var prevPosX;
    var prevPosY;
    var prevTime;

    var handleMouseMove = function(ev) {
        // update pad position
        var newX = ev.clientX - canvas.offsetLeft - sp.padPlayerLeft.getW()/2;
        newX = Math.max(0, newX);
        newX = Math.min(newX, sp.table.padZoneH - sp.padPlayerLeft.getW());
        sp.padPlayerLeft.x = newX;
        var newY = ev.clientY - canvas.offsetTop - sp.padPlayerLeft.getH()/2;
        newY = Math.max(0, newY);
        newY = Math.min(newY, sp.table.h - sp.padPlayerLeft.getH());
        sp.padPlayerLeft.y = newY;
        
        // update pad speed
        var now = Date.now();
        if (prevPosX !== undefined && prevTime !== undefined){
            sp.padPlayerLeft.vx = (newX - prevPosX) / (now - prevTime);
        }
        if (prevPosY !== undefined && prevTime !== undefined) {
            sp.padPlayerLeft.vy = (newY- prevPosY) / (now - prevTime);
        }
        
        prevPosX = newX;
        prevPosY = newY
        prevTime = now;
    };

    var keyPressMoveOffset = 20;

    var left = 37,
        right = 39,
        up = 38,
        down = 40;
    var handleKeyPress = function(ev) {
        if (ev.keyCode == up) {
            var newY = sp.padPlayerRight.y - keyPressMoveOffset;
            newY = Math.max(0, newY);
            newY = Math.min(newY, sp.table.h - sp.padPlayerRight.getH());
            sp.padPlayerRight.y = newY;
        } else if (ev.keyCode == down) {
            var newY = sp.padPlayerRight.y + keyPressMoveOffset;
            newY = Math.max(0, newY);
            newY = Math.min(newY, sp.table.h - sp.padPlayerRight.getH());
            sp.padPlayerRight.y = newY;
        } else if (ev.keyCode == left) {
            var newX = sp.padPlayerRight.x - keyPressMoveOffset;
            newX = Math.max(sp.table.w - sp.table.padZoneH, newX);
            newX = Math.min(newX, sp.table.w - sp.padPlayerRight.getW());
            sp.padPlayerRight.x = newX;
        } else if (ev.keyCode == right) {
            // todo do anything?
        }
    };
    var handleKeyUp = function(ev) {
        if (ev.keyCode == left) {
            sp.padPlayerRight.x = sp.table.w - sp.padPlayerRight.getW();
        }
    };

    var start = function() {
        document.addEventListener('mousemove', handleMouseMove);
        document.addEventListener('keypress', handleKeyPress);
        document.addEventListener('keyup', handleKeyUp);
    };

    var stop = function() {
        document.removeEventListener('mousemove', handleMouseMove);
        document.removeEventListener('keypress', handleKeyPress);
        document.removeEventListener('keyup', handleKeyUp);
    };

    return {
        start: start,
        stop: stop
    };

};
