/* global shufflepuck */
/* global directions */
shufflepuck = {};

document.addEventListener('DOMContentLoaded', function(){

    var canvas = document.getElementById('canvas');

    var initGame = function(boardHeigth, boardWidth, hightPad, radiusPuck, padZoneHeigth, frameRate){
        
        if (boardHeigth === undefined) {
            boardHeigth = 480;
        }
        
        if (boardWidth === undefined){
            boardWidth = 640
        }
        
        if (padZoneHeigth === undefined){
            padZoneHeigth = boardHeigth / 4;
        }
        
        if (hightPad === undefined) {
            hightPad = boardWidth / 5;
        }
        
        if (radiusPuck === undefined) {
            radiusPuck = hightPad / 6;
        }
        
        if (frameRate === undefined) {
            frameRate = 60;
        }
        
        var getHeight = function (){
            return hightPad;
        }
    
        var getWidth = function () {
            return hightPad / 4;
        }
        
        var sp = {};
        
        sp.frameRate = frameRate;

        sp.table = {
            h : boardHeigth,
            w : boardWidth,
            padZoneH : padZoneHeigth
        }

        sp.padPlayerLeft = {
            x : 0,
            y : sp.table.h/2 - getHeight()/2,
            vx : 0,
            vy : 0,
            getH : getHeight,
            getW : getWidth,
            
            getXBorderMax : function () {
                return this.x + this.getW();
            },
            
            getXBorderMin : function () {
                return this.x;
            },
            
            getYBorderMax : function () {
                return this.y + this.getH();
            },
            
            getYBorderMin : function () {
                return this.y;
            },
            
        };
        
        sp.padPlayerRight = {
            x : sp.table.w - getWidth(),
            y : sp.table.h/2 - getHeight()/2,
            vx : 0,
            vy : 0,
            getH : getHeight,
            getW : getWidth,
            
            getXBorderMax : function () {
                return this.x + this.getW();
            },
            
            getXBorderMin : function () {
                return this.x;
            },
            
            getYBorderMax : function () {
                return this.y + this.getH();
            },
            
            getYBorderMin : function () {
                return this.y;
            }
            
            
        }
        
        sp.puck = {
            x : 0,
            y : 0,
            vx : 0,
            vy : 0,
            getR : function() {return radiusPuck},
            
            getXBorderMax : function () {
                return this.x + this.getR();
            },
            
            getXBorderMin : function () {
                return this.x - this.getR();
            },
            
            getYBorderMax : function () {
                return this.y + this.getR();
            },
            
            getYBorderMin : function () {
                return this.y - this.getR();
            }
        }

        sp.renderer = rendering(canvas, sp);
        sp.ai = initAi(sp);

        sp.eventHandler = handleEvents(canvas, sp);
        sp.eventHandler.start();

        
        return sp;
        
    }
    
    shufflepuck = initGame();
    
    var updateGame = function(){
        shufflepuck.renderer.draw();
        shufflepuck.ai.calculateGame();
    }
    
    var renderLoop = window.setInterval(updateGame, 1000 / shufflepuck.frameRate);
    
}, false);



