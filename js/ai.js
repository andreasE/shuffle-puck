initAi = function(sp) {
    
    var Directions = {
        left : "left",
        right : "right",
        up : "up",
        down : "down",
        none : "none"
    }
    
    // remove later
    // hack puck
    // speed in pixel / frame
    sp.puck.vx = 5;
    sp.puck.vy = 3;
    sp.puck.x = sp.table.w / 2;
    sp.puck.y = sp.table.h / 2;
    
    var update = function() {
        updatePuck()
    };
    
    var direction = function(object) {
        if (object.vx !== undefined && object.vy !== undefined) {
            var directionX = object.vx > 0 ? Directions.right :
                                             (object.vx < 0 ? Directions.left : Directions.none);
            var directionY = object.vy > 0 ? Directions.down : (object.vy < 0 ? Directions.up : Directions.none);
            return {
                x : directionX,
                y : directionY
            }
        } else {
            return {
                x : undefined,
                y : undefined
            }
        }
    }
    
    var updatePuck = function () {
        tableCollision();
        var pad = padCollision();
        if (pad !== undefined) {
            // puck bounces off (with slightly decreased speed)
            sp.puck.vx = sp.puck.vx * (0.9);
            sp.puck.vy = sp.puck.vy * (0.9);
            // speed of pad added to puck
            sp.puck.vx += pad.vx;
            sp.puck.vy += pad.vy;
            
            var dirPuck = direction(sp.puck);
            var dirPad = direction(pad);
            if (dirPuck.x != dirPad.x) {
                // change direction of puck
                sp.puck.vx = sp.puck.vx * (-1);
                // reposition puck
                if (dirPuck.x == Directions.right){
                    sp.puck.x = pad.getXBorderMin() - sp.puck.getR();
                } else if (dirPuck.x == Directions.left) {
                    sp.puck.x = pad.getXBorderMax() + sp.puck.getR();
                }
            } else {
                // reposition puck
                if (dirPuck.x == Directions.left){
                    sp.puck.x = pad.getXBorderMin() - sp.puck.getR();
                } else if (dirPuck.x == Directions.right) {
                    sp.puck.x = pad.getXBorderMax() + sp.puck.getR();
                }
            }
            
        }
        
        sp.puck.x += sp.puck.vx;
        sp.puck.y += sp.puck.vy;
    }
    
    var padCollision = function() {
        if (padCollisionForPad(sp.padPlayerLeft)) {
            return sp.padPlayerLeft;
        } else if (padCollisionForPad(sp.padPlayerRight)) {
            return sp.padPlayerRight;
        }
    }
    
    var padCollisionForPad = function(pad) {
        // wenn rechts größer min und links kleiner max BOOOM
        return (sp.puck.getXBorderMax() >= pad.getXBorderMin()) &&
            (sp.puck.getXBorderMin() <= pad.getXBorderMax()) &&
            (sp.puck.getYBorderMax() >= pad.getYBorderMin()) &&
            (sp.puck.getYBorderMin() <= pad.getYBorderMax());
        
    }
    
    var tableCollision = function(){
        if (collisionOnX()){
            sp.puck.vx = sp.puck.vx * (-1);
        } else if (collisionOnY()){
            sp.puck.vy = sp.puck.vy * (-1);
        }
    }
    
    var collisionOnX = function(){
        return (sp.puck.getXBorderMax() >= sp.table.w) ||
           (sp.puck.getXBorderMin() <= 0);
    }
    
    var collisionOnY = function(){
        return (sp.puck.getYBorderMax() >= sp.table.h) ||
           (sp.puck.getYBorderMin() <= 0)
    }

    return {
        calculateGame: update
    };

};